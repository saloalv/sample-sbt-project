import scala.io.StdIn

object CommandLineApplication {
  def main(args: Array[String]): Unit = {
    val name = StdIn.readLine("Please enter your name: ")
    println(generateHello(name))
  }

  def generateHello(name: String): String = {
    s"Hello $name!"
  }
}
