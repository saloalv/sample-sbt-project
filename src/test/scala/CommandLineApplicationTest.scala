import org.scalatest.FlatSpec
import CommandLineApplication.generateHello

class CommandLineApplicationTest extends FlatSpec {

  "A generated hello" should "contain the name of the person greeted" in {
    val name = "TestName"
    val generatedHello = generateHello(name)
    assert(generatedHello contains name)
  }

  it should "start with 'Hello'" in {
    val name = "TestName"
    val generatedHello = generateHello(name)
    assert(generatedHello startsWith "Hello")
  }



}
