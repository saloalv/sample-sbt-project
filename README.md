# Sample SBT project with automatic builds

This is just a project to try out the following technologies:

* **Scala**: Writing a whole application in scala
* **Git**: Using a branching model similar to git-flow
  * Include stuff like a license file, contributing guidelines, a readme that explains how to build and run
* **Gitlab**: Utilizing Gitlab CI/CD to provide automated builds, testing, and packaging
  * Testing using **scala-test**
  * **SBT** for builds
  * Including scala-library in SBT builds using **sbt-assembly**
   in order to allow running the application using plain java
* **ScalaFX** for GUI