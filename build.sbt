name := "sample-sbt-project"

/*
Version numbering starts with a manually bumped version number, currently both major and minor.
Followed by branch/tag and short form of commit SHA.
 */
version := "0.1-" + sys.env.get("CI_COMMIT_REF_NAME") + "-" + sys.env.getOrElse("CI_COMMIT_SHORT_SHA", "SNAPSHOT")

scalaVersion := "2.12.8"

libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % "test"

logBuffered in Test := false